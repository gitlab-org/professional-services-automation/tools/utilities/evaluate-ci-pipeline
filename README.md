# Running Evaluate in a CI Pipeline

## Usage

- Create a new project on your GitLab instance
- Set the following CI variables in the project
    - TOKEN: The admin token used to run evaluate
    - HOST: The host of the GitLab instance (ex: `https://gitlab.example.com`)
    - NUM_PROCESSES: Number of processes Evaluate will use. By default, set this to `4`
- Copy the `.gitlab-ci.yml` file from this repo to the new GitLab project

